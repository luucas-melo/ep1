#include "Cliente.hpp"
#include <string>

fstream arquivo;
Cliente::Cliente(){
    set_nome("");
    set_cpf("");
    set_telefone("");
    set_email("");
    set_socio(false);
}
Cliente::Cliente(string cpf,string nome,string telefone, string email){
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    set_socio(false);
}

Cliente::~Cliente(){
}
string Cliente::get_nome(){
    return nome;
}
void Cliente::set_nome(string nome){
    this-> nome = nome;
}

string Cliente::get_cpf(){
    return cpf;
}

void Cliente::set_cpf(string cpf){
    this-> cpf = cpf;
}

string Cliente::get_telefone(){
    return telefone;
}

void Cliente::set_telefone(string telefone){
    this->telefone = telefone;
}
string Cliente::get_email(){
    return email;
}

void Cliente::set_email(string email){
    this-> email = email;
}

bool Cliente::get_socio(){
    return socio;
}

void Cliente::set_socio(bool socio){
    this-> socio  = socio;
}
    
   


