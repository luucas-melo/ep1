#include "Carrinho.hpp"
#include "Estoque.hpp"
#include "Cliente.hpp"
#include "Modo.hpp"

using namespace std;


void Carrinho::colocar_carrinho(Cliente cliente, string codigo,int qtd){
    fstream arquivo;
    string temp;
    vector <string> vetor;
    vector <string> produtos_carrinho;
    vector <string> clientes;
    int linha= Estoque::verifica_codigo(codigo);
    arquivo.open("Estoque.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
    }
    arquivo.close();
    arquivo.open("Carrinho.txt", ios::in);
        while(getline(arquivo,temp)){
            produtos_carrinho.push_back(temp);
        }
    arquivo.close();
    produtos_carrinho.push_back(vetor[linha]);
    produtos_carrinho.push_back(vetor[linha+1]);
    produtos_carrinho.push_back(vetor[linha+2]);
    produtos_carrinho.push_back(vetor[linha+4]);
    produtos_carrinho.push_back(to_string(qtd));
    produtos_carrinho.push_back("---------");

    arquivo.open("Cliente.txt", ios::in);
    while(getline(arquivo,temp)){
        clientes.push_back(temp);
    }
    arquivo.close();
    arquivo.open("Carrinho.txt", ios::out | ios::trunc);
    for(auto i=(produtos_carrinho.begin());i<(produtos_carrinho.end());i++)
        arquivo<<*i<<endl;
    arquivo.close();

}

float Carrinho::preco_total(){
    float total=0;
    fstream arquivo;
    vector <string> vetor;
    string temp;
    arquivo.open("Carrinho.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
    }
    arquivo.close();
    for(size_t i=4;i<(vetor.size());i+=6){
        total+=(stof(vetor[i])*stof(vetor[i-1]));
    }
    return total;
}

void Carrinho::finaliza_carrinho(){
    fstream arquivo;
    arquivo.open("Carrinho.txt", ios::out | ios::trunc);
    arquivo.close();
}
