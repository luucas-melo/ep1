#include "Produto.hpp"



Produto::Produto(){
    set_codigo("0000");
    set_nome("");
    set_categoria(" ");
    set_preco(0.0);
    set_qtd(0);
}

Produto::Produto(string codigo,string nome,string categoria,float preco,int qtd){
    set_codigo(codigo);
    set_nome(nome);
    set_categoria(categoria);
    set_preco(preco);
    set_qtd(qtd);
}

Produto::~Produto(){
}

string Produto::get_codigo(){
    return codigo;
}
void Produto::set_codigo(string codigo){
    this->codigo = codigo;
}

string Produto::get_nome(){
    return nome;
}
void Produto::set_nome(string nome){
    this-> nome = nome;
}

string Produto::get_categoria(){
    return categoria;
}

void Produto::set_categoria(string categoria){
    this->categoria = categoria;
}

int Produto::get_qtd(){
    return qtd;
}

void Produto::set_qtd(int qtd){
    this-> qtd = qtd;
}

double Produto::get_preco(){
    return preco;
}

void Produto::set_preco(double preco){
    this->preco = preco;
}

