#include "Modo.hpp"
#include <iostream>
#include <cctype>
#include "Cliente.hpp"
#include "Estoque.hpp"
#include "Carrinho.hpp"
#include <stdlib.h>
void menu_estoque(){
    cout << "-----Modo Estoque-----" << endl;
    cout << "1 - cadastrar produto" << endl;
    cout << "2 - mudar a quantidade do produto" << endl;
    cout << "3 - Listar produto" << endl;
    cout << "4 - Sair" << endl;
}
void menu_principal(){
    cout << "-----Mercado da Vitória-----" << endl;
    cout << "1 - Modo Venda" << endl;
    cout << "2 - Modo Estoque" << endl;
    cout << "3 - Modo Modo Recomendacao" << endl;
    cout << "4 - Sair" << endl;
}

using namespace std;
void Modo::modo_venda(){
    system ("clear");
    string cpf;
    char continuar='s';
    char op ='s';
    char op_compra ='s';
    Carrinho::finaliza_carrinho();
    cout <<"-----Modo Venda-----"<<endl;
    cout <<"Digite o cpf do cliente" <<endl;
    cin.ignore();
    getline(cin,cpf);
    loop:
    int verifica_cpf =Modo::verifica_cliente(cpf);
    if(verifica_cpf==-1){
        cout << "Cliente não cadastrado"<<endl;
        cout << "Deseja cadastrar um novo cliente [S], [N]";
        cin >> op;
        op = tolower(op);      
        if(op=='s'){
           // cout << "SIM" <<endl;
            cadastra_cliente(cpf);
            cout<<"Deseja comprar[s/n]?"<<endl;
            cin >>op_compra;
            op_compra =tolower(op_compra);
            if(op_compra=='s')
                goto comprar;
            else
                cout<<"Programa finalizado"<<endl;  
       
        }
        else if(op =='n'){
            cout<<"Programa finalizado"<<endl;     
        }
        else{
            cout <<"Opcão inválida: Digite [S] para CADASTRAR cliente ou [N] para NAO CADASTRAR";
            goto loop;
        }
       
    }
    else{
        comprar:
        fstream arquivo;
        string temp;
        vector <string> vetor;
        vector <string> quantidade;
        Cliente cliente;
        arquivo.open("Clientes.txt", ios::in);
        while(getline(arquivo,temp)){
        vetor.push_back(temp);
        }
        arquivo.close();
        for(auto i=(vetor.begin());i<(vetor.end());i+=6){
            if(cpf==*i){
            string nome = *(i+1);
            string telefone = *(i+2);
            string email = *(i+3);
            cliente.set_cpf(cpf);
            cliente.set_nome(nome);
            cliente.set_telefone(telefone);
            cliente.set_email(email);
            }
        }
        do{
        vetor.clear();

        string codigo;
        int qtd_comp;
        int qtd;
        system("clear");
        Estoque::lista_estoque();
        cout<<"========Comprar produto:========"<<endl;
        cout<<"Digite o codigo do produto: "<<endl;
        cin >>codigo;
        
        arquivo.open("Estoque.txt", ios::in);
        while(getline(arquivo,temp)){
            quantidade.push_back(temp);
        }
        arquivo.close();
        int linha_codigo=Estoque::verifica_codigo(codigo);
        
        if(linha_codigo==-1){
            cout<< "Produto não encontrado"<<endl;
            goto comprar;
        }
        else{
            cout<< "comprando o produto "<< quantidade[linha_codigo+1]<<endl;
            for(auto i=(quantidade.begin());i<(quantidade.end());i+=6){
                if(codigo==*i){
                qtd =atoi((quantidade[linha_codigo+1]).c_str());    
                }
            }
        
                cout<<"Digite a quantidade a ser comprada: "<<endl;
                cin>> qtd_comp;
                Estoque::mudar_qtd(qtd,qtd_comp,linha_codigo);
                Carrinho::colocar_carrinho(cliente,codigo,qtd_comp);
    }
        if(qtd_comp<=0){
                    cout<<"Quantidade invalida!"<<endl;
                }
    cout<<"O carrinho possui R$:"<<Carrinho::preco_total()<<" em compras"<<endl;
    cout<<"Deseja continuar comprando?[s/n]"<<endl;
    
    cin>>continuar;
    if(continuar=='n'){
        system("clear");
        Modo::modo_estoque();
    }
}while(continuar=='s');
    }
}

void Modo::modo_estoque(){
    loop:
    menu_estoque();
    int op;
    int linha;
    string codigo;
    int nova_qtd;
    cin >> op;
    switch(op){
        case 1: 
            cout << "======cadastrar produto======" << endl;
            Estoque::cadastra_produto();
            break;
        case 2: 
            cout << "======mudar a quantidade do produto======" << endl;
            cout<< "Digite o código"<<endl;
            cin >>codigo;
            linha = Estoque::verifica_codigo(codigo);
            if(linha==-1){
                cout<<"Produto inexistente"<<endl;
            }
            else{
                cout<<"Digite a nova quantidade"<<endl;
                cin>>nova_qtd;
                Estoque::mudar_qtd(nova_qtd,linha);
            }
    
            break;
        case 3: 
            cout << "======Listar estoque======" << endl;
            Estoque::lista_estoque();
            system("clear");
            goto loop;
            break;
        case 4: 
            break;
        default:
            cout << "Opcão inválida"<<endl;
            cout << "Selecione novamente"<<endl;
            goto loop;  
    }


}

void Modo::modo_recomendacao(){

}
int Modo::verifica_cliente(string cpf){
    string temp;
    vector <string> vetor;
    int aux=0;
    fstream arquivo;
    arquivo.open("Clientes.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
        if(vetor[aux]==cpf){
            arquivo.close();
            return aux;
        }
        aux++;
    }
    arquivo.close();
    return -1;
}

void Modo::cadastra_cliente(string cpf){
    Cliente cliente;
    using namespace std;
    string nome,email,temp,telefone;
    fstream arquivo;
    vector <string> vetor;
    
    cout << "######CADASTRO DE CLIENTES######" <<endl;
    cliente.set_cpf(cpf);
    cin.ignore();
    cout << "Digite o nome do novo cliente"<<endl;
    getline(cin,nome);
    cliente.set_nome(nome);
    
    cout << "Digite o email" << endl;
    getline(cin,email);
    cliente.set_email(email);

    cout << "Digite o telefone" << endl;
    getline(cin,telefone);
    cliente.set_telefone(telefone);

    arquivo.open("Clientes.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
    }
    arquivo.close();
    vetor.push_back(cpf);
    vetor.push_back(nome);
    vetor.push_back(email);
    vetor.push_back(telefone);
    vetor.push_back("---------");

    arquivo.open("Clientes.txt",ios::out | ios::app);
    for(auto i=(vetor.begin());i!=(vetor.end());i++){
        arquivo<<*i<<endl;
    }
    arquivo.close();
    vetor.clear();
    
    cout<<"O cliente foi cadastrado"<<endl;
}

