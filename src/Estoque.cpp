
#include "Estoque.hpp"
#include <iostream>
#include "Modo.hpp"
#include "Produto.hpp"
#include <fstream>
#include <vector>


void Estoque::cadastra_produto(){
    loop:
   
    fstream arquivo("Estoque.txt");
    vector <string> vetor;
    string codigo, nome, categoria,preco,qtd,temp;
    cout<<"Digite o codigo do produto: ";
    cin.ignore();
    getline(cin,codigo);
    
    int verifica_codigo = Estoque::verifica_codigo(codigo);
    if(verifica_codigo==-1){
        Produto produto;
        produto.set_codigo(codigo);
        cout<<"Digite aqui o Nome do produto: ";
        getline(cin,nome);
        produto.set_nome(nome);

        cout<<"Digite as categorias do produto: ";
        getline(cin,categoria);
        produto.set_categoria(categoria);
        cout<<"Digite o valor do produto: R$";
        getline(cin,preco);
        produto.set_preco(atof(preco.c_str()));
        
        cout<<"Digite a quantidade disponivel: ";
        getline(cin,qtd);
        produto.set_qtd(atoi(qtd.c_str()));

       

        arquivo.open("Estoque.txt", ios::in);

        while(getline(arquivo,temp)){
            vetor.push_back(temp);
        }
        arquivo.close();
    
        
        vetor.push_back(codigo);
        vetor.push_back(nome);
        vetor.push_back(categoria);
        vetor.push_back(preco);
        vetor.push_back(qtd);
        vetor.push_back("---------");
        arquivo.open("Estoque.txt",ios::out | ios::app);
        for(auto i=(vetor.begin());i!=(vetor.end());i++){
            arquivo<<*i<<endl;       
        }
        arquivo.close();
        vetor.clear();
        cout<<"O produto foi cadastrado"<<endl;
    }
    else{
        cout << "Codigo do produto já cadastrado"<< endl;
        goto loop;
    }
}


int Estoque::verifica_codigo(string codigo){
    string temp;
    vector <string> vetor;
    int aux=0;
    fstream arquivo;
    arquivo.open("Estoque.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
        if(vetor[aux]==codigo){
            arquivo.close();
            return aux;
        }
        aux++;
    }
    arquivo.close();
    return -1;
}

void Estoque::lista_estoque(){
    fstream arquivo;
    string temp;
    vector <string> estoque;
    estoque.clear();
    arquivo.open("Estoque.txt", ios::in);
    while(getline(arquivo,temp)){
        estoque.push_back(temp);
    }
    arquivo.close();
    cout<<"============Produtos do Estoque============"<<endl;
    for(auto i=(estoque.begin());i<(estoque.end());i+=6){
        cout<<"Codigo: "<<*i<<endl;
        cout<<"Nome: "<<*(i+1)<<endl;
        cout<<"Categoria: "<<*(i+2)<<endl;
        cout<<"Preco: "<<*(i+3)<<endl;
        cout<<"Quantidade: "<<*(i+4)<<endl;
        cout << "---------"<<endl;
    }
}

void Estoque::mudar_qtd(int nova_qtd,int linha){
    vector <string> vetor;
    string temp;
    int qtd;
    fstream arquivo;
    arquivo.open("Estoque.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
    }
    arquivo.close();
    linha+=4;
    qtd=atoi(vetor[linha].c_str());
    qtd=nova_qtd;
    vetor[linha]=to_string(qtd);
    arquivo.open("Estoque.txt", ios::out | ios::trunc);
    for(auto j=(vetor.begin());j!=(vetor.end());j++){
        arquivo<<*j<<endl;
    }
    cout<<"Quantidade atualizada"<<endl;
    arquivo.close();
    vetor.clear();

}
//sobrecarga de mudar_qtd
void Estoque::mudar_qtd(int qtd,int nova_qtd,int linha){
    vector <string> vetor;
    string temp;
    fstream arquivo;
    vetor.clear();
    arquivo.open("Estoque.txt", ios::in);
    while(getline(arquivo,temp)){
        vetor.push_back(temp);
    }
    arquivo.close();
    linha+=4;
    qtd=atoi(vetor[linha].c_str());
    if(qtd-nova_qtd<0){
        cout<<"Quantidade superior a do estoque"<<endl;
    }
    else{
        qtd=qtd-nova_qtd;
        vetor[linha]=to_string(qtd);
        arquivo.open("Estoque.txt", ios::out | ios::trunc);
        for(auto j=(vetor.begin());j!=(vetor.end());j++){
            arquivo<<*j<<endl;
        }   
        cout<<"Quantidade do estoque atualizada"<<endl;
        cout<<qtd<<endl;
        arquivo.close();
        vetor.clear();
    }

}


