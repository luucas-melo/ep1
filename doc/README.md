# EP1 - OO 2019.2 (UnB - Gama)

## Descrição

Victoria possui um pequeno comércio que atende a população de seu bairro. Com o passar do tempo, a pequena empreendedora foi adquirindo experiência e, por conta de seu excelente poder de negociação, ela conseguia reduzir significativamente o preço dos produtos oferecidos.

Entretanto, apenas preços baixos não eram o bastante para manter a clientela. Em uma noite de inspiração, Victoria pensou em duas estratégias para atrair mais pessoas:
- Oferecer descontos de 15% para clientes sócios;
- Oferecer produtos recomendados exclusivamente para cada cliente;

Para colocar as ideias em prática, ela deve abandonar seu velho hábito de utilizar seu estimado caderninho para gerenciar sua Modo. Uma amiga te recomendou para desenvolver um *software* que ajude o estabelecimento a implementar as novas estratégias de negócio. Com a sua ajuda, **todas as vendas serão realizadas pelo computador operado por uma funcionária**. Em uma breve conversa com Victoria, foi possível entender algumas características importantes do sistema:
- Victoria está aprendendo C++ em um curso online e, portanto, prefere que o sistema seja feito nesta linguagem para que ela consiga fazer as próprias manutenções quando necessário;
- Devem existir três modos de operação do sistema:
    - **Modo venda**
    - **Modo recomendação**
    - **Modo estoque**

### Modo venda
	Incompleto:
		-sem desconto para socio;
		-usuario pode ser cadastrado, caso digite um cpf novo;
        -cpf já cadastrado: 999999999

### Modo estoque
Incompleto:
	Não se pode escolher mais de uma categoria;

### Modo estoque
Não foi implementado

## Orientações


