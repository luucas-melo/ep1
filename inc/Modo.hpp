#ifndef MODO_HPP
#define MODO_HPP
#include <vector>
#include<Produto.hpp>
#include<iostream>
#include<string>
#include <fstream>
class Modo{
public:
    static void modo_estoque(); 
    static void modo_venda(); 
    static void modo_recomendacao();
    static void cadastra_cliente(string cpf); 
    static int verifica_cliente(string cpf); 
};

#endif