#include <iostream>
#include <string>
#include "Produto.hpp"
#include "vector"

using namespace std;

class Estoque{
private:
    int tam_estoque=0;
    Estoque();
    ~Estoque();
public: 
    static void cadastra_produto(); 
    static int verifica_codigo(string codigo);
    static void lista_estoque();
    static void mudar_qtd(int nova_qtd,int linha);
    static void mudar_qtd(int qtd,int nova_qtd,int linha);
};