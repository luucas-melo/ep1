#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
class Cliente{
    private:
        string nome;
        string cpf;
        string telefone;
        string email;
        bool socio;
    
    public:
        Cliente(); 
        Cliente(string cpf,string nome,string telefone, string email);
        ~Cliente(); 
        void set_nome(string nome);
        string get_nome();
        void set_cpf(string cpf);
        string get_cpf();
        void set_telefone(string telefone);
        string get_telefone();
        void set_email(string email);
        string get_email();
        void set_socio(bool socio);
        bool get_socio();
};
#endif
