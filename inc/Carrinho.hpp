#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include <fstream>
#include<vector>
#include<iostream>
#include "Produto.hpp"
#include "Cliente.hpp"

class Carrinho{
    public:
        static void colocar_carrinho(Cliente cliente,string codigo,int qtd);
        static float preco_total();
        static void finaliza_carrinho();
};

#endif