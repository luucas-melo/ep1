#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include <string>
#include <iostream>

using namespace std;
class Produto{
    private:
        string codigo;
        string nome;
        string categoria;
        int qtd;
        double preco;
    public:
        Produto(); 
        Produto(string codigo, string nome, string categoria, float preco, int qtd);
        ~Produto(); 
        void set_codigo(string codigo);
        string get_codigo();
        void set_nome(string nome);
        string get_nome();
        void set_categoria(string categoria);
        string get_categoria();
        void set_qtd(int qtd);
        int get_qtd();
        void set_preco(double preco);
        double get_preco();
       
        void imprime_dados();
        void cadastrar_produto();

        
};
#endif